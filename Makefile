OUTDIR = public
STATIC_DEST = $(patsubst static/%,$(OUTDIR)/%,$(wildcard static/*))
HTML_PAGES = $(addprefix $(OUTDIR)/,index.html $(addsuffix /index.html,mods \
	       client-mods texture-packs minetest-game))
DATE = $(shell date +'%-d %b %Y')
API_PAGE_FLAGS = --template templates/api-page.html --lua-filter util/title.lua \
	-M date:"$(DATE)" --toc --toc-depth 3 --indented-code-classes lua \
	--highlight-style highlight.theme
PDF_FLAGS = --template templates/lua-api.tex --lua-filter util/title.lua \
	-M date:"$(DATE)" --toc --toc-depth 2 --indented-code-classes lua \
	--highlight-style highlight.theme --pdf-engine xelatex

API_URL = https://github.com/minetest/minetest/blob/master/doc/lua_api.txt
CSM_API_URL = https://github.com/minetest/minetest/blob/master/doc/client_lua_api.txt
TEXTURES_URL = https://github.com/minetest/minetest/blob/master/doc/texture_packs.txt
MTG_API_URL = https://github.com/minetest/minetest_game/blob/master/game_api.txt
API_RAW_URL = $(subst github,raw.githubusercontent,$(subst blob/,,$(API_URL)))
CSM_API_RAW_URL = $(subst github,raw.githubusercontent,$(subst blob/,,$(CSM_API_URL)))
TEXTURES_RAW_URL = $(subst github,raw.githubusercontent,$(subst blob/,,$(TEXTURES_URL)))
MTG_API_RAW_URL = $(subst github,raw.githubusercontent,$(subst blob/,,$(MTG_API_URL)))

.PHONY: all
all: fetch-latest pages pdf

.PHONY: fetch-latest
fetch-latest:
	wget -q $(API_RAW_URL) -O pages/lua_api.txt
	wget -q $(CSM_API_RAW_URL) -O pages/client_lua_api.txt
	wget -q $(TEXTURES_RAW_URL) -O pages/texture_packs.txt
	wget -q $(MTG_API_RAW_URL) -O pages/game_api.txt
	printf "%s\n" 4,6m22 w | ed -s pages/lua_api.txt

.PHONY: pages
pages: $(STATIC_DEST) $(HTML_PAGES)

.PHONY: pdf
pdf: $(OUTDIR)/lua-api.pdf

.PHONY: deploy-local
deploy-local:
	cd $(OUTDIR) && python3 -m http.server

$(OUTDIR)/index.html: pages/index.md templates/index.html | $(OUTDIR)
	pandoc --template templates/index.html -M date:"$(DATE)" $< -o $@

$(OUTDIR)/mods/index.html: pages/lua_api.txt templates/api-page.html
	@mkdir -p $(@D)
	pandoc $(API_PAGE_FLAGS) -M filename:"$(<F)" -M fileurl:"$(API_URL)" \
		-M pdffile:"../lua-api.pdf" --base-header-level 2 $< -o $@

$(OUTDIR)/client-mods/index.html: pages/client_lua_api.txt templates/api-page.html
	@mkdir -p $(@D)
	pandoc $(API_PAGE_FLAGS) -M filename:"$(<F)" -M fileurl:"$(CSM_API_URL)" $< -o $@

$(OUTDIR)/texture-packs/index.html: pages/texture_packs.txt templates/api-page.html
	@mkdir -p $(@D)
	pandoc $(API_PAGE_FLAGS) -M filename:"$(<F)" -M fileurl:"$(TEXTURES_URL)" $< -o $@

$(OUTDIR)/minetest-game/index.html: pages/game_api.txt templates/api-page.html
	@mkdir -p $(@D)
	pandoc $(API_PAGE_FLAGS) -M filename:"$(<F)" -M fileurl:"$(MTG_API_URL)" $< -o $@

$(OUTDIR)/lua-api.pdf: pages/lua_api.txt templates/lua-api.tex | $(OUTDIR)
	pandoc $(PDF_FLAGS) $< -o $@

$(OUTDIR)/%: static/% | $(OUTDIR)
	cp $< $@

$(OUTDIR):
	mkdir $(OUTDIR)

.PHONY: clean
clean:
	rm -rf $(OUTDIR)
